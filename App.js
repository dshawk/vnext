/* eslint-disable prettier/prettier */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import { SafeAreaView, StyleSheet, ScrollView, View, Text, StatusBar, TextInput, TouchableOpacity, Image } from 'react-native';

import Modal from 'react-native-modal';
import items from './items';

import { Colors } from 'react-native/Libraries/NewAppScreen';

import {
  ViroVRSceneNavigator,
  ViroARSceneNavigator
} from 'react-viro';

import imgInfo from './js/res/1-11.jpg';
import logoAr from './js/res/ar.png';
import notification from './js/res/notification.png';
import InitialARScene from './js/HelloWorldSceneAR';
import InitialVRScene from './js/ViroMediaPlayer/Viro360Theatre';

const ChatItems = ({ item, ...props }) => {
  if (item.type === 'other') {
    return (
      <View style={{ marginBottom: 24 }}>
        <View style={{ flexDirection: 'row' }}>
          <View style={styles.imageItems} />
          <Text style={{ alignSelf: 'center', marginHorizontal: 24, width: 200 }}>{item.title}</Text>
        </View>
      </View>)
  }
  return (
    <View style={{ marginBottom: 24 }}>
      <View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>

        <Text style={{ alignSelf: 'center', marginHorizontal: 24 }}>{item.title}</Text>
        <View style={styles.imageItems} />
      </View>
    </View>
  )

}

const Info = ({ item, exit, ...props }) => {
  const [chat, setChat] = useState([
    { title: 'Привет, о супер будет новая площадка', type: 'other' },
    { title: 'А когда будет готова?', type: 'other' },
    { title: 'Сейчас узнаю', type: 'you' }])
  return (
    <View style={{ paddingTop: 20, flex: 1 }}>
      <Text style={{ fontSize: 24 }}>Спортивная площадка</Text>
      <Image source={imgInfo} style={{
        width: '100%', height: 128,
        resizeMode: 'cover', marginBottom: 24, marginTop: 8
      }} />
      <ScrollView>
        {chat.map(item => {
          return (<ChatItems item={item} />)
        })}
      </ScrollView>
      <TouchableOpacity onPress={exit} style={{ ...styles.logInBtn, width: '100%', alignSelf: 'flex-end' }}>
        <Text style={{ color: '#fff' }}>Выйти</Text>
      </TouchableOpacity>
    </View>
  )
}

const CellItems = ({ item, onPress = () => { }, ...props }) => {

  return (
    <TouchableOpacity style={styles.itemsCell} onPress={onPress}>
      <View style={{ flexDirection: 'row' }}>
        <View style={styles.imageItems} />
        <Text style={{ alignSelf: 'center', marginHorizontal: 24, color: '#1A3951', fontSize: 14 }}>{item.title}</Text>
      </View>
      <Text style={{ marginTop: 24, color: '#1A3951', opacity: 0.5, fontSize: 12 }}>{item.descr}</Text>
    </TouchableOpacity>
  );
};

const App: () => React$Node = () => {
  const [showLogIn, setshowLogIn] = useState(true);
  const [typeUse, setTypeUse] = useState('');
  const [info, setInfo] = useState(false)
  if (typeUse === 'ar') {
    return (
      <View style={{ flex: 1 }}>
        <ViroARSceneNavigator initialScene={{ scene: InitialARScene }} />
        <View style={{
          width: '100%', alignSelf: 'flex-end', paddingHorizontal: 20,
          backgroundColor: '#005697',
        }}>
          <TouchableOpacity onPress={() => { setTypeUse('') }} style={{ ...styles.logInBtn, width: '100%', }}>
            <Text style={{ color: '#fff' }}>Выйти</Text>
          </TouchableOpacity></View>
      </View>)
  }
  /*
    if (typeUse === 'vr') {
  
      return (
        <View style={{ flex: 1 }}>
          <ViroVRSceneNavigator initialScene={{ scene: InitialVRScene }} />
          <View style={{
            width: '100%', alignSelf: 'flex-end', paddingHorizontal: 20,
            backgroundColor: '#005697',
          }}>
            <TouchableOpacity onPress={() => { setTypeUse('') }} style={{ ...styles.logInBtn, width: '100%', }}>
              <Text style={{ color: '#fff' }}>Выйти</Text>
            </TouchableOpacity></View>
        </View>)
    } */
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Modal isVisible={showLogIn} style={styles.modalContener}>
          <View style={styles.contentCenter}>
            <View style={styles.imageLogin} />
            <TouchableOpacity onPress={() => setshowLogIn(false)} style={styles.logInBtn}>
              <Text style={{ color: '#fff' }}>Войти через гос услуги</Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <Modal isVisible={info} style={styles.modalContener}>
          <View style={styles.contentItems}>
            <Info exit={() => setInfo(false)} />
          </View>

        </Modal>
        <Modal isVisible={false} style={styles.modalContener}>

        </Modal>
        <View style={{ height: 44, backgroundColor: '#fff', flexDirection: 'row', justifyContent: 'space-between' }} >

          <TouchableOpacity style={{ width: 44, height: 44, paddingHorizontal: 24 }} onPress={() => { setTypeUse('vr') }}>
            <Image source={notification} style={{ width: 36, height: 36, alignSelf: 'center', marginTop: 4 }} />
          </TouchableOpacity>
          <Text style={{ fontSize: 24, alignSelf: 'center' }} >Демо</Text>
          <TouchableOpacity style={{ paddingHorizontal: 8 }} onPress={() => { setTypeUse('ar') }}>
            <Image source={logoAr} style={{ width: 44, height: 44, alignSelf: 'center' }} />
          </TouchableOpacity>
        </View>
        <ScrollView contentInsetAdjustmentBehavior="automatic" style={styles.scrollView}>
          {items.map(item => {
            return <CellItems item={item} onPress={() => setInfo(true)} />;
          })}
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const stylesShadow = StyleSheet.create({
  shadowOpacity: 0.3,
  shadowRadius: 3,
  shadowOffset: {
    height: 0,
    width: 0,
  },
  elevation: 1,
});

const styles = StyleSheet.create({
  modalContener: {
    flex: 1,
    margin: 0,
  },
  itemsCell: {
    ...stylesShadow,
    backgroundColor: '#fff',
    borderRadius: 20,
    padding: 24,
    marginVertical: 8
  },
  logInBtn: {
    borderRadius: 12,
    height: 44,
    width: 220,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
    backgroundColor: '#005697',
  },
  contentCenter: {
    height: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  contentItems: {
    height: '100%',
    backgroundColor: '#fff',
    padding: 24
  },
  imageLogin: {
    backgroundColor: 'red',
    height: 128,
    width: 128,
    borderRadius: 69,
    marginBottom: 24,
  },
  imageItems: {
    backgroundColor: 'red',
    height: 44,
    width: 44,
    borderRadius: 22,
    alignSelf: 'center'
  },
  scrollView: {
    height: '100%',
    backgroundColor: '#EEF7FC',
    paddingHorizontal: 20,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
