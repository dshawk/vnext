module.exports = {
  printWidth: 120,
  tabWidth: 4,
  useTabs: true,
  bracketSpacing: true,
  jsxBracketSameLine: false,
  arrowParens: 'avoid',
  singleQuote: true
};
